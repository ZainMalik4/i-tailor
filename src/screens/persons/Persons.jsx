import { View, Text } from "react-native";
import React from "react";
import { personStyles } from "./personStyles";

export default function Persons({ route }) {
  const data = route.params;

  return (
    <View style={personStyles.container}>
      <Text style={personStyles.label}>Persons: {data.name}</Text>
      <Text style={personStyles.label}>Age: {data.age}</Text>
    </View>
  );
}
