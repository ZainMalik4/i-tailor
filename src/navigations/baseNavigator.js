import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Register from "../screens/Register";
import Tailors from "../screens/Tailors";
import Persons from "../screens/persons/Persons";

const BaseStack = createNativeStackNavigator();

function BaseNavigator() {
  return (
    <NavigationContainer>
      <BaseStack.Navigator
        screenOptions={{
          headerTitleStyle: {
            color: "orange",
            fontFamily: "OperatorMono-Book",
          },
          headerSearchBarOptions: {
            placeholder: "search me",
          },
        }}
      >
        <BaseStack.Screen name={"Register"} component={Register} />
        <BaseStack.Screen name={"Tailors"} component={Tailors} />
        <BaseStack.Screen name={"Persons"} component={Persons} />
      </BaseStack.Navigator>
    </NavigationContainer>
  );
}

export { BaseNavigator };
